//express configuration
var express = require('express');
var app = express();
var serv = require('http').Server(app);

//if user goes localhost:2000, send them to index.html
app.get('/',function(req, res)
{
	res.sendFile(__dirname + '/client/index.html');
	//res.sendFile(__dirname + '/client/css/style.css');
});
app.use('/client',express.static(__dirname + '/client'));

//listen to port 2000
serv.listen(2000);

//LAN Connectivity
//serv.listen(9000, '192.168.100.135');

//log that the server started
console.log('Server Started');

//Connection List
var SOCKET_LIST = {};

var QUESTION_LIST = [{
		q: "1 + 1",
		a: "2"
	}, {
		q: "2 + 2",
		a: "4"
	}, {
		q: "3 + 3",
		a: "6"
	}, {
		q: "4 + 4",
		a: "8"
	}, {
		q: "5 + 5",
		a: "10"
	}, {
		q: "6 + 6",
		a: "12"
	}, {
		q: "7 + 7",
		a: "14"
	}, {
		q: "8 + 8",
		a: "16"
	}, {
		q: "9 + 9",
		a: "18"
	}, {
		q: "10 + 10",
		a: "20"
	}, ];


var Entity = function()
{
	var self = {
		position: 0,
		questionNum: 0,
		userid: 1,
		won: false,
		rate: .5,
		enter: false
	}
	self.update = function(){
		self.updatePosition();
	}
	
	self.updatePosition = function()
	{
		if(self.enter)
		{
			self.enter = false;
			self.position += self.rate;
		}
	}

	return self;
}


//Player Object
var Player = function(id)
{
	var self = Entity();
	
	self.id = id;
	self.userid = id;

	var super_update = self.update;
	self.update = function()
	{
		super_update();
	}
	
	Player.list[id] = self;
	return self;
};

//Player List
Player.list = {};

//On Connect, Creates a new player based on Socket ID
Player.onConnect = function(socket)
{
	//create player
	var player = Player(socket.id);
}

//On player disconnect, this function is called
Player.onDisconnect = function(socket)
{
	delete Player.list[socket.id];
}

Player.update = function()
{
	//create a package to store all the players
	var pack = [];
	
	//Loop through all connected Sockets/Users
	for(var i in Player.list)
	{
		//set socket to equal each specific user
		var player = Player.list[i];
		
		//updates off of the entity function
		player.update();
		
		//Push the data to the package
		pack.push({

			question:player.questionNum,
			position:player.position,
			userid:player.userid,
			status:player.won,
			questionslist:QUESTION_LIST
		});
	}
	return pack;
}

var io = require('socket.io')(serv,{});

//on connecting, run this function
io.sockets.on('connection', function(socket)
{
	socket.id = Math.random();


	SOCKET_LIST[socket.id] = socket;
	Player.onConnect(socket);
	
	//On disconnect, remove the player and socket from their lists
	socket.on('disconnect',function()
	{
		delete SOCKET_LIST[socket.id];
		Player.onDisconnect(socket);
	});
	
	socket.on('evalResponse', function(data)
	{
		player = Player(socket.id);
		//rate = .5;
		//console.log(data);
		//console.log(player.questionNum);
		if(QUESTION_LIST[player.questionNum].a === data.theirResponse)
		{
			//player.position += player.rate;
			player.enter = data.state;
		}
		else
		{
			//player.position = player.position;
		}
		return player;
	});
});



//Main Game Loops
//Loop at 60 frames per second, or 40ms
setInterval(function()
{
	var pack = Player.update();
	
	
	for(var i in SOCKET_LIST)
	{
		//set socket to equal each specific user
		var socket = SOCKET_LIST[i];
		
		//send the package to the client with all player information
		socket.emit('usersConnected',pack);
		//socket.emit('questions',questionsList);
	}
},1000/24);

